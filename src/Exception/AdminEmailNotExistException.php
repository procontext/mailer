<?php

namespace Procontext\Mailer\Exception;

use Throwable;

class AdminEmailNotExistException extends MailerException
{
    public function __construct($message = "Email администратора не задан", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}