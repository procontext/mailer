<?php

namespace Procontext\Mailer;

use Procontext\Mailer\Exception\EmailNotExistException;
use Procontext\Mailer\Exception\PasswordNotExistException;
use Swift_SmtpTransport;
use Swift_Mailer;
use Swift_Message;

class SyncSender
{
    private $mailer;
    private $email;

    public function __construct()
    {
        $host = env('MAIL_HOST', 'smtp.yandex.ru');
        $port = env('MAIL_PORT', 465);
        $encryption = env('MAIL_ENCRYPTION', 'ssl');
        $email = env('MAIL_EMAIL');
        $password = env('MAIL_PASSWORD');

        if(!$email) {
            throw new EmailNotExistException();
        }

        if(!$password) {
            throw new PasswordNotExistException();
        }

        $transport = new Swift_SmtpTransport($host, $port, $encryption);
        $transport->setUsername($email)
            ->setPassword($password);

        $this->email = $email;
        $this->mailer = new Swift_Mailer($transport);
    }

    public function send(string $title, string $subject, string $body, array $recipients): bool
    {
        $message = new Swift_Message();
        $message->setFrom($this->email, $title);
        $message->setTo($recipients);
        $message->setSubject($subject);
        $message->setBody($body, 'text/html');

        return (bool) $this->mailer->send($message);
    }
}